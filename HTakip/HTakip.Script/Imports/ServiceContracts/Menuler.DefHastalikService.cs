﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Imported, PreserveMemberCase]
    public partial class DefHastalikService
    {
        [InlineConstant] public const string BaseUrl = "Menuler/DefHastalik";

        [InlineCode("Q.serviceRequest('Menuler/DefHastalik/Create', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Create(SaveRequest<DefHastalikRow> request, Action<SaveResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/DefHastalik/Update', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Update(SaveRequest<DefHastalikRow> request, Action<SaveResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }
 
        [InlineCode("Q.serviceRequest('Menuler/DefHastalik/Delete', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Delete(DeleteRequest request, Action<DeleteResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/DefHastalik/Retrieve', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Retrieve(RetrieveRequest request, Action<RetrieveResponse<DefHastalikRow>> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/DefHastalik/List', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest List(ListRequest request, Action<ListResponse<DefHastalikRow>> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [Imported, PreserveMemberCase]
        public static class Methods
        {
            [InlineConstant] public const string Create = "Menuler/DefHastalik/Create";
            [InlineConstant] public const string Update = "Menuler/DefHastalik/Update";
            [InlineConstant] public const string Delete = "Menuler/DefHastalik/Delete";
            [InlineConstant] public const string Retrieve = "Menuler/DefHastalik/Retrieve";
            [InlineConstant] public const string List = "Menuler/DefHastalik/List";
        }
    }
}