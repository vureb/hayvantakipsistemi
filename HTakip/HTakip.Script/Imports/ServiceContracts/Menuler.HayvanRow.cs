﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Imported, Serializable, PreserveMemberCase]
    public partial class HayvanRow
    {
        [InlineConstant] public const string IdProperty = "IdHayvan";
        [InlineConstant] public const string NameProperty = "KupeNo";
        [InlineConstant] public const string LocalTextPrefix = "Menuler.Hayvan";

        public Int32? IdHayvan { get; set; }
        public String KupeNo { get; set; }
        public Int32? Cinsiyet { get; set; }
        public Int32? IdHayvanTuruRef { get; set; }
        public Int32? IdHayvanCinsiRef { get; set; }
        public Boolean? SatildiDurumu { get; set; }
        public Boolean? KesildiDurumu { get; set; }
        public Boolean? TelefDurumu { get; set; }
        public String Aciklama { get; set; }
        public String HayvanDogumTarihi { get; set; }
        public Int32? PadokNo { get; set; }
        public Int32? IdHayvanCinsiRefIdHayvanTuruRef { get; set; }
        public String IdHayvanCinsiRefHayvanCinsiAdi { get; set; }

        [Imported, PreserveMemberCase]
        public static class Fields
        {
            [InlineConstant] public const string IdHayvan = "IdHayvan";
            [InlineConstant] public const string KupeNo = "KupeNo";
            [InlineConstant] public const string Cinsiyet = "Cinsiyet";
            [InlineConstant] public const string IdHayvanTuruRef = "IdHayvanTuruRef";
            [InlineConstant] public const string IdHayvanCinsiRef = "IdHayvanCinsiRef";
            [InlineConstant] public const string SatildiDurumu = "SatildiDurumu";
            [InlineConstant] public const string KesildiDurumu = "KesildiDurumu";
            [InlineConstant] public const string TelefDurumu = "TelefDurumu";
            [InlineConstant] public const string Aciklama = "Aciklama";
            [InlineConstant] public const string HayvanDogumTarihi = "HayvanDogumTarihi";
            [InlineConstant] public const string PadokNo = "PadokNo";
            [InlineConstant] public const string IdHayvanCinsiRefIdHayvanTuruRef = "IdHayvanCinsiRefIdHayvanTuruRef";
            [InlineConstant] public const string IdHayvanCinsiRefHayvanCinsiAdi = "IdHayvanCinsiRefHayvanCinsiAdi";
        }
    }
}