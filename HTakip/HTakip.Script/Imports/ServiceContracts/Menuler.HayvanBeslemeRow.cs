﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Imported, Serializable, PreserveMemberCase]
    public partial class HayvanBeslemeRow
    {
        [InlineConstant] public const string IdProperty = "IdBesleme";
        [InlineConstant] public const string LocalTextPrefix = "Menuler.HayvanBesleme";

        public Int32? IdBesleme { get; set; }
        public Int32? IdHayvanRef { get; set; }
        public Int32? IdYemRef { get; set; }
        public String BeslemeTarihi { get; set; }
        public String IdHayvanRefYemAdi { get; set; }

        [Imported, PreserveMemberCase]
        public static class Fields
        {
            [InlineConstant] public const string IdBesleme = "IdBesleme";
            [InlineConstant] public const string IdHayvanRef = "IdHayvanRef";
            [InlineConstant] public const string IdYemRef = "IdYemRef";
            [InlineConstant] public const string BeslemeTarihi = "BeslemeTarihi";
            [InlineConstant] public const string IdHayvanRefYemAdi = "IdHayvanRefYemAdi";
        }
    }
}