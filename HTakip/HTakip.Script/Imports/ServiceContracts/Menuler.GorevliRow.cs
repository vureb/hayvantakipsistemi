﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Imported, Serializable, PreserveMemberCase]
    public partial class GorevliRow
    {
        [InlineConstant] public const string IdProperty = "IdGorevli";
        [InlineConstant] public const string NameProperty = "Ad";
        [InlineConstant] public const string LocalTextPrefix = "Menuler.Gorevli";

        public Int32? IdGorevli { get; set; }
        public Int32? Yetki { get; set; }
        public Int32? Tc { get; set; }
        public String Ad { get; set; }
        public String Soyad { get; set; }
        public String Adres { get; set; }
        public Int32? Telefon { get; set; }
        public String Mail { get; set; }
        public Boolean? IstenCiktiMi { get; set; }

        [Imported, PreserveMemberCase]
        public static class Fields
        {
            [InlineConstant] public const string IdGorevli = "IdGorevli";
            [InlineConstant] public const string Yetki = "Yetki";
            [InlineConstant] public const string Tc = "Tc";
            [InlineConstant] public const string Ad = "Ad";
            [InlineConstant] public const string Soyad = "Soyad";
            [InlineConstant] public const string Adres = "Adres";
            [InlineConstant] public const string Telefon = "Telefon";
            [InlineConstant] public const string Mail = "Mail";
            [InlineConstant] public const string IstenCiktiMi = "IstenCiktiMi";
        }
    }
}