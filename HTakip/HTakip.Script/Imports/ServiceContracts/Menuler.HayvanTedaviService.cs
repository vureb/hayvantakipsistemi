﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Imported, PreserveMemberCase]
    public partial class HayvanTedaviService
    {
        [InlineConstant] public const string BaseUrl = "Menuler/HayvanTedavi";

        [InlineCode("Q.serviceRequest('Menuler/HayvanTedavi/Create', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Create(SaveRequest<HayvanTedaviRow> request, Action<SaveResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/HayvanTedavi/Update', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Update(SaveRequest<HayvanTedaviRow> request, Action<SaveResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }
 
        [InlineCode("Q.serviceRequest('Menuler/HayvanTedavi/Delete', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Delete(DeleteRequest request, Action<DeleteResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/HayvanTedavi/Retrieve', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Retrieve(RetrieveRequest request, Action<RetrieveResponse<HayvanTedaviRow>> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/HayvanTedavi/List', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest List(ListRequest request, Action<ListResponse<HayvanTedaviRow>> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [Imported, PreserveMemberCase]
        public static class Methods
        {
            [InlineConstant] public const string Create = "Menuler/HayvanTedavi/Create";
            [InlineConstant] public const string Update = "Menuler/HayvanTedavi/Update";
            [InlineConstant] public const string Delete = "Menuler/HayvanTedavi/Delete";
            [InlineConstant] public const string Retrieve = "Menuler/HayvanTedavi/Retrieve";
            [InlineConstant] public const string List = "Menuler/HayvanTedavi/List";
        }
    }
}