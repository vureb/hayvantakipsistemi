﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Imported, PreserveMemberCase]
    public partial class HayvanNitelikleriService
    {
        [InlineConstant] public const string BaseUrl = "Menuler/HayvanNitelikleri";

        [InlineCode("Q.serviceRequest('Menuler/HayvanNitelikleri/Create', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Create(SaveRequest<HayvanNitelikleriRow> request, Action<SaveResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/HayvanNitelikleri/Update', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Update(SaveRequest<HayvanNitelikleriRow> request, Action<SaveResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }
 
        [InlineCode("Q.serviceRequest('Menuler/HayvanNitelikleri/Delete', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Delete(DeleteRequest request, Action<DeleteResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/HayvanNitelikleri/Retrieve', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Retrieve(RetrieveRequest request, Action<RetrieveResponse<HayvanNitelikleriRow>> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/HayvanNitelikleri/List', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest List(ListRequest request, Action<ListResponse<HayvanNitelikleriRow>> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [Imported, PreserveMemberCase]
        public static class Methods
        {
            [InlineConstant] public const string Create = "Menuler/HayvanNitelikleri/Create";
            [InlineConstant] public const string Update = "Menuler/HayvanNitelikleri/Update";
            [InlineConstant] public const string Delete = "Menuler/HayvanNitelikleri/Delete";
            [InlineConstant] public const string Retrieve = "Menuler/HayvanNitelikleri/Retrieve";
            [InlineConstant] public const string List = "Menuler/HayvanNitelikleri/List";
        }
    }
}