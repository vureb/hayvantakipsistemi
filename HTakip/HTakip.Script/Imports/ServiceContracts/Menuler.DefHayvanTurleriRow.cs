﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Imported, Serializable, PreserveMemberCase]
    public partial class DefHayvanTurleriRow
    {
        [InlineConstant] public const string IdProperty = "IdHayvanTuru";
        [InlineConstant] public const string NameProperty = "HayvanTuruAdi";
        [InlineConstant] public const string LocalTextPrefix = "Menuler.DefHayvanTurleri";

        public Int32? IdHayvanTuru { get; set; }
        public String HayvanTuruAdi { get; set; }

        [Imported, PreserveMemberCase]
        public static class Fields
        {
            [InlineConstant] public const string IdHayvanTuru = "IdHayvanTuru";
            [InlineConstant] public const string HayvanTuruAdi = "HayvanTuruAdi";
        }
    }
}