﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Imported, Serializable, PreserveMemberCase]
    public partial class AsilamaRow
    {
        [InlineConstant] public const string IdProperty = "IdAsilama";
        [InlineConstant] public const string LocalTextPrefix = "Menuler.Asilama";

        public Int32? IdAsilama { get; set; }
        public Int32? IdHayvanRef { get; set; }
        public String AsilamaTarihi { get; set; }
        public Int32? IdGorevliRef { get; set; }
        public Boolean? AsilamaYapildimi { get; set; }
        public Int32? IdAsiRef { get; set; }
        public String IdHayvanRefKupeNo { get; set; }
        public Int32? IdHayvanRefCinsiyet { get; set; }
        public Int32? IdHayvanRefIdHayvanTuruRef { get; set; }
        public Int32? IdHayvanRefIdHayvanCinsiRef { get; set; }
        public Boolean? IdHayvanRefSatildiDurumu { get; set; }
        public Boolean? IdHayvanRefKesildiDurumu { get; set; }
        public Boolean? IdHayvanRefTelefDurumu { get; set; }
        public String IdHayvanRefAciklama { get; set; }
        public String IdHayvanRefHayvanDogumTarihi { get; set; }
        public Int32? IdHayvanRefPadokNo { get; set; }
        public Int32? IdGorevliRefYetki { get; set; }
        public Int32? IdGorevliRefTc { get; set; }
        public String IdGorevliRefAd { get; set; }
        public String IdGorevliRefSoyad { get; set; }
        public String IdGorevliRefAdres { get; set; }
        public Int32? IdGorevliRefTelefon { get; set; }
        public String IdGorevliRefMail { get; set; }
        public Boolean? IdGorevliRefIstenCiktiMi { get; set; }
        public String IdAsiRefAsiAdi { get; set; }

        [Imported, PreserveMemberCase]
        public static class Fields
        {
            [InlineConstant] public const string IdAsilama = "IdAsilama";
            [InlineConstant] public const string IdHayvanRef = "IdHayvanRef";
            [InlineConstant] public const string AsilamaTarihi = "AsilamaTarihi";
            [InlineConstant] public const string IdGorevliRef = "IdGorevliRef";
            [InlineConstant] public const string AsilamaYapildimi = "AsilamaYapildimi";
            [InlineConstant] public const string IdAsiRef = "IdAsiRef";
            [InlineConstant] public const string IdHayvanRefKupeNo = "IdHayvanRefKupeNo";
            [InlineConstant] public const string IdHayvanRefCinsiyet = "IdHayvanRefCinsiyet";
            [InlineConstant] public const string IdHayvanRefIdHayvanTuruRef = "IdHayvanRefIdHayvanTuruRef";
            [InlineConstant] public const string IdHayvanRefIdHayvanCinsiRef = "IdHayvanRefIdHayvanCinsiRef";
            [InlineConstant] public const string IdHayvanRefSatildiDurumu = "IdHayvanRefSatildiDurumu";
            [InlineConstant] public const string IdHayvanRefKesildiDurumu = "IdHayvanRefKesildiDurumu";
            [InlineConstant] public const string IdHayvanRefTelefDurumu = "IdHayvanRefTelefDurumu";
            [InlineConstant] public const string IdHayvanRefAciklama = "IdHayvanRefAciklama";
            [InlineConstant] public const string IdHayvanRefHayvanDogumTarihi = "IdHayvanRefHayvanDogumTarihi";
            [InlineConstant] public const string IdHayvanRefPadokNo = "IdHayvanRefPadokNo";
            [InlineConstant] public const string IdGorevliRefYetki = "IdGorevliRefYetki";
            [InlineConstant] public const string IdGorevliRefTc = "IdGorevliRefTc";
            [InlineConstant] public const string IdGorevliRefAd = "IdGorevliRefAd";
            [InlineConstant] public const string IdGorevliRefSoyad = "IdGorevliRefSoyad";
            [InlineConstant] public const string IdGorevliRefAdres = "IdGorevliRefAdres";
            [InlineConstant] public const string IdGorevliRefTelefon = "IdGorevliRefTelefon";
            [InlineConstant] public const string IdGorevliRefMail = "IdGorevliRefMail";
            [InlineConstant] public const string IdGorevliRefIstenCiktiMi = "IdGorevliRefIstenCiktiMi";
            [InlineConstant] public const string IdAsiRefAsiAdi = "IdAsiRefAsiAdi";
        }
    }
}