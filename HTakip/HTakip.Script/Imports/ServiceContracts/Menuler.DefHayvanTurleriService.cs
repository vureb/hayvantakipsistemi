﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Imported, PreserveMemberCase]
    public partial class DefHayvanTurleriService
    {
        [InlineConstant] public const string BaseUrl = "Menuler/DefHayvanTurleri";

        [InlineCode("Q.serviceRequest('Menuler/DefHayvanTurleri/Create', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Create(SaveRequest<DefHayvanTurleriRow> request, Action<SaveResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/DefHayvanTurleri/Update', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Update(SaveRequest<DefHayvanTurleriRow> request, Action<SaveResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }
 
        [InlineCode("Q.serviceRequest('Menuler/DefHayvanTurleri/Delete', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Delete(DeleteRequest request, Action<DeleteResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/DefHayvanTurleri/Retrieve', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Retrieve(RetrieveRequest request, Action<RetrieveResponse<DefHayvanTurleriRow>> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/DefHayvanTurleri/List', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest List(ListRequest request, Action<ListResponse<DefHayvanTurleriRow>> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [Imported, PreserveMemberCase]
        public static class Methods
        {
            [InlineConstant] public const string Create = "Menuler/DefHayvanTurleri/Create";
            [InlineConstant] public const string Update = "Menuler/DefHayvanTurleri/Update";
            [InlineConstant] public const string Delete = "Menuler/DefHayvanTurleri/Delete";
            [InlineConstant] public const string Retrieve = "Menuler/DefHayvanTurleri/Retrieve";
            [InlineConstant] public const string List = "Menuler/DefHayvanTurleri/List";
        }
    }
}