﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Imported, Serializable, PreserveMemberCase]
    public partial class HayvanNitelikleriRow
    {
        [InlineConstant] public const string IdProperty = "IdNitelik";
        [InlineConstant] public const string LocalTextPrefix = "Menuler.HayvanNitelikleri";

        public Int32? IdNitelik { get; set; }
        public Int32? IdHayvanRef { get; set; }
        public String OlcumTarihi { get; set; }
        public Double? Agirlik { get; set; }
        public Double? Boyu { get; set; }
        public String IdHayvanRefKupeNo { get; set; }
        public Int32? IdHayvanRefCinsiyet { get; set; }
        public Int32? IdHayvanRefIdHayvanTuruRef { get; set; }
        public Int32? IdHayvanRefIdHayvanCinsiRef { get; set; }
        public Boolean? IdHayvanRefSatildiDurumu { get; set; }
        public Boolean? IdHayvanRefKesildiDurumu { get; set; }
        public Boolean? IdHayvanRefTelefDurumu { get; set; }
        public String IdHayvanRefAciklama { get; set; }
        public String IdHayvanRefHayvanDogumTarihi { get; set; }
        public Int32? IdHayvanRefPadokNo { get; set; }

        [Imported, PreserveMemberCase]
        public static class Fields
        {
            [InlineConstant] public const string IdNitelik = "IdNitelik";
            [InlineConstant] public const string IdHayvanRef = "IdHayvanRef";
            [InlineConstant] public const string OlcumTarihi = "OlcumTarihi";
            [InlineConstant] public const string Agirlik = "Agirlik";
            [InlineConstant] public const string Boyu = "Boyu";
            [InlineConstant] public const string IdHayvanRefKupeNo = "IdHayvanRefKupeNo";
            [InlineConstant] public const string IdHayvanRefCinsiyet = "IdHayvanRefCinsiyet";
            [InlineConstant] public const string IdHayvanRefIdHayvanTuruRef = "IdHayvanRefIdHayvanTuruRef";
            [InlineConstant] public const string IdHayvanRefIdHayvanCinsiRef = "IdHayvanRefIdHayvanCinsiRef";
            [InlineConstant] public const string IdHayvanRefSatildiDurumu = "IdHayvanRefSatildiDurumu";
            [InlineConstant] public const string IdHayvanRefKesildiDurumu = "IdHayvanRefKesildiDurumu";
            [InlineConstant] public const string IdHayvanRefTelefDurumu = "IdHayvanRefTelefDurumu";
            [InlineConstant] public const string IdHayvanRefAciklama = "IdHayvanRefAciklama";
            [InlineConstant] public const string IdHayvanRefHayvanDogumTarihi = "IdHayvanRefHayvanDogumTarihi";
            [InlineConstant] public const string IdHayvanRefPadokNo = "IdHayvanRefPadokNo";
        }
    }
}