﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Imported, PreserveMemberCase]
    public partial class HayvanBeslemeService
    {
        [InlineConstant] public const string BaseUrl = "Menuler/HayvanBesleme";

        [InlineCode("Q.serviceRequest('Menuler/HayvanBesleme/Create', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Create(SaveRequest<HayvanBeslemeRow> request, Action<SaveResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/HayvanBesleme/Update', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Update(SaveRequest<HayvanBeslemeRow> request, Action<SaveResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }
 
        [InlineCode("Q.serviceRequest('Menuler/HayvanBesleme/Delete', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Delete(DeleteRequest request, Action<DeleteResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/HayvanBesleme/Retrieve', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Retrieve(RetrieveRequest request, Action<RetrieveResponse<HayvanBeslemeRow>> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/HayvanBesleme/List', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest List(ListRequest request, Action<ListResponse<HayvanBeslemeRow>> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [Imported, PreserveMemberCase]
        public static class Methods
        {
            [InlineConstant] public const string Create = "Menuler/HayvanBesleme/Create";
            [InlineConstant] public const string Update = "Menuler/HayvanBesleme/Update";
            [InlineConstant] public const string Delete = "Menuler/HayvanBesleme/Delete";
            [InlineConstant] public const string Retrieve = "Menuler/HayvanBesleme/Retrieve";
            [InlineConstant] public const string List = "Menuler/HayvanBesleme/List";
        }
    }
}