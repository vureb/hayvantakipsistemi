﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Imported, PreserveMemberCase]
    public partial class DefHayvanCinsiService
    {
        [InlineConstant] public const string BaseUrl = "Menuler/DefHayvanCinsi";

        [InlineCode("Q.serviceRequest('Menuler/DefHayvanCinsi/Create', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Create(SaveRequest<DefHayvanCinsiRow> request, Action<SaveResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/DefHayvanCinsi/Update', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Update(SaveRequest<DefHayvanCinsiRow> request, Action<SaveResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }
 
        [InlineCode("Q.serviceRequest('Menuler/DefHayvanCinsi/Delete', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Delete(DeleteRequest request, Action<DeleteResponse> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/DefHayvanCinsi/Retrieve', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest Retrieve(RetrieveRequest request, Action<RetrieveResponse<DefHayvanCinsiRow>> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [InlineCode("Q.serviceRequest('Menuler/DefHayvanCinsi/List', {request}, {onSuccess}, {options})")]
        public static jQueryXmlHttpRequest List(ListRequest request, Action<ListResponse<DefHayvanCinsiRow>> onSuccess, ServiceCallOptions options = null)
        {
            return null;
        }

        [Imported, PreserveMemberCase]
        public static class Methods
        {
            [InlineConstant] public const string Create = "Menuler/DefHayvanCinsi/Create";
            [InlineConstant] public const string Update = "Menuler/DefHayvanCinsi/Update";
            [InlineConstant] public const string Delete = "Menuler/DefHayvanCinsi/Delete";
            [InlineConstant] public const string Retrieve = "Menuler/DefHayvanCinsi/Retrieve";
            [InlineConstant] public const string List = "Menuler/DefHayvanCinsi/List";
        }
    }
}