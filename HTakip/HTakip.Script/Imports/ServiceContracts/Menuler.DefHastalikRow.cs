﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Imported, Serializable, PreserveMemberCase]
    public partial class DefHastalikRow
    {
        [InlineConstant] public const string IdProperty = "IdHastalik";
        [InlineConstant] public const string NameProperty = "HastalikAdi";
        [InlineConstant] public const string LocalTextPrefix = "Menuler.DefHastalik";

        public Int32? IdHastalik { get; set; }
        public String HastalikAdi { get; set; }
        public String Aciklama { get; set; }

        [Imported, PreserveMemberCase]
        public static class Fields
        {
            [InlineConstant] public const string IdHastalik = "IdHastalik";
            [InlineConstant] public const string HastalikAdi = "HastalikAdi";
            [InlineConstant] public const string Aciklama = "Aciklama";
        }
    }
}