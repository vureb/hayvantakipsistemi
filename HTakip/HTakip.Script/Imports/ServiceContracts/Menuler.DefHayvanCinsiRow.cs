﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Imported, Serializable, PreserveMemberCase]
    public partial class DefHayvanCinsiRow
    {
        [InlineConstant] public const string IdProperty = "IdHayvanCinsi";
        [InlineConstant] public const string NameProperty = "HayvanCinsiAdi";
        [InlineConstant] public const string LocalTextPrefix = "Menuler.DefHayvanCinsi";

        public Int32? IdHayvanCinsi { get; set; }
        public Int32? IdHayvanTuruRef { get; set; }
        public String HayvanCinsiAdi { get; set; }
        public String IdHayvanTuruRefHayvanTuruAdi { get; set; }

        [Imported, PreserveMemberCase]
        public static class Fields
        {
            [InlineConstant] public const string IdHayvanCinsi = "IdHayvanCinsi";
            [InlineConstant] public const string IdHayvanTuruRef = "IdHayvanTuruRef";
            [InlineConstant] public const string HayvanCinsiAdi = "HayvanCinsiAdi";
            [InlineConstant] public const string IdHayvanTuruRefHayvanTuruAdi = "IdHayvanTuruRefHayvanTuruAdi";
        }
    }
}