﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Imported, Serializable, PreserveMemberCase]
    public partial class DefAsilarRow
    {
        [InlineConstant] public const string IdProperty = "IdAsi";
        [InlineConstant] public const string NameProperty = "AsiAdi";
        [InlineConstant] public const string LocalTextPrefix = "Menuler.DefAsilar";

        public Int32? IdAsi { get; set; }
        public String AsiAdi { get; set; }

        [Imported, PreserveMemberCase]
        public static class Fields
        {
            [InlineConstant] public const string IdAsi = "IdAsi";
            [InlineConstant] public const string AsiAdi = "AsiAdi";
        }
    }
}