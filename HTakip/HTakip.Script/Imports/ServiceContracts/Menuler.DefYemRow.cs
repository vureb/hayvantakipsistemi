﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Imported, Serializable, PreserveMemberCase]
    public partial class DefYemRow
    {
        [InlineConstant] public const string IdProperty = "IdYem";
        [InlineConstant] public const string NameProperty = "YemAdi";
        [InlineConstant] public const string LocalTextPrefix = "Menuler.DefYem";

        public Int32? IdYem { get; set; }
        public String YemAdi { get; set; }

        [Imported, PreserveMemberCase]
        public static class Fields
        {
            [InlineConstant] public const string IdYem = "IdYem";
            [InlineConstant] public const string YemAdi = "YemAdi";
        }
    }
}