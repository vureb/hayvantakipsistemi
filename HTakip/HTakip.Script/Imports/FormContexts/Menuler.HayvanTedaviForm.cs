﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public partial class HayvanTedaviForm : PrefixedContext
    {
        [InlineConstant] public const string FormKey = "Menuler.HayvanTedavi";

        public HayvanTedaviForm(string idPrefix) : base(idPrefix) {}


        public IntegerEditor IdHayvanRef { get { return ById<IntegerEditor>("IdHayvanRef"); } }
        public DateEditor TedaviTarihi { get { return ById<DateEditor>("TedaviTarihi"); } }
        public IntegerEditor IdHastalikRef { get { return ById<IntegerEditor>("IdHastalikRef"); } }
        public StringEditor Aciklama { get { return ById<StringEditor>("Aciklama"); } }
    }
}