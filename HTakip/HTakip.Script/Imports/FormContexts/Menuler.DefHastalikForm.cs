﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public partial class DefHastalikForm : PrefixedContext
    {
        [InlineConstant] public const string FormKey = "Menuler.DefHastalik";

        public DefHastalikForm(string idPrefix) : base(idPrefix) {}


        public StringEditor HastalikAdi { get { return ById<StringEditor>("HastalikAdi"); } }
        public StringEditor Aciklama { get { return ById<StringEditor>("Aciklama"); } }
    }
}