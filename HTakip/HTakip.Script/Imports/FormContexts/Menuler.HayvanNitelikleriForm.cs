﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public partial class HayvanNitelikleriForm : PrefixedContext
    {
        [InlineConstant] public const string FormKey = "Menuler.HayvanNitelikleri";

        public HayvanNitelikleriForm(string idPrefix) : base(idPrefix) {}


        public IntegerEditor IdHayvanRef { get { return ById<IntegerEditor>("IdHayvanRef"); } }
        public DateEditor OlcumTarihi { get { return ById<DateEditor>("OlcumTarihi"); } }
        public DecimalEditor Agirlik { get { return ById<DecimalEditor>("Agirlik"); } }
        public DecimalEditor Boyu { get { return ById<DecimalEditor>("Boyu"); } }
    }
}