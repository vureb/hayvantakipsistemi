﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public partial class HayvanForm : PrefixedContext
    {
        [InlineConstant] public const string FormKey = "Menuler.Hayvan";

        public HayvanForm(string idPrefix) : base(idPrefix) {}


        public StringEditor KupeNo { get { return ById<StringEditor>("KupeNo"); } }
        public IntegerEditor Cinsiyet { get { return ById<IntegerEditor>("Cinsiyet"); } }
        public IntegerEditor IdHayvanTuruRef { get { return ById<IntegerEditor>("IdHayvanTuruRef"); } }
        public IntegerEditor IdHayvanCinsiRef { get { return ById<IntegerEditor>("IdHayvanCinsiRef"); } }
        public BooleanEditor SatildiDurumu { get { return ById<BooleanEditor>("SatildiDurumu"); } }
        public BooleanEditor KesildiDurumu { get { return ById<BooleanEditor>("KesildiDurumu"); } }
        public BooleanEditor TelefDurumu { get { return ById<BooleanEditor>("TelefDurumu"); } }
        public StringEditor Aciklama { get { return ById<StringEditor>("Aciklama"); } }
        public DateEditor HayvanDogumTarihi { get { return ById<DateEditor>("HayvanDogumTarihi"); } }
        public IntegerEditor PadokNo { get { return ById<IntegerEditor>("PadokNo"); } }
    }
}