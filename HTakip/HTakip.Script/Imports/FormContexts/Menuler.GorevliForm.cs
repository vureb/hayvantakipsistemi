﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public partial class GorevliForm : PrefixedContext
    {
        [InlineConstant] public const string FormKey = "Menuler.Gorevli";

        public GorevliForm(string idPrefix) : base(idPrefix) {}


        public IntegerEditor Yetki { get { return ById<IntegerEditor>("Yetki"); } }
        public IntegerEditor Tc { get { return ById<IntegerEditor>("Tc"); } }
        public StringEditor Ad { get { return ById<StringEditor>("Ad"); } }
        public StringEditor Soyad { get { return ById<StringEditor>("Soyad"); } }
        public StringEditor Adres { get { return ById<StringEditor>("Adres"); } }
        public IntegerEditor Telefon { get { return ById<IntegerEditor>("Telefon"); } }
        public StringEditor Mail { get { return ById<StringEditor>("Mail"); } }
        public BooleanEditor IstenCiktiMi { get { return ById<BooleanEditor>("IstenCiktiMi"); } }
    }
}