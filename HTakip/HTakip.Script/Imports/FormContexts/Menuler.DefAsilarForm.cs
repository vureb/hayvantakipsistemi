﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public partial class DefAsilarForm : PrefixedContext
    {
        [InlineConstant] public const string FormKey = "Menuler.DefAsilar";

        public DefAsilarForm(string idPrefix) : base(idPrefix) {}


        public StringEditor AsiAdi { get { return ById<StringEditor>("AsiAdi"); } }
    }
}