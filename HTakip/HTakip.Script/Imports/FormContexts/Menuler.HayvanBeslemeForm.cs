﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public partial class HayvanBeslemeForm : PrefixedContext
    {
        [InlineConstant] public const string FormKey = "Menuler.HayvanBesleme";

        public HayvanBeslemeForm(string idPrefix) : base(idPrefix) {}


        public IntegerEditor IdHayvanRef { get { return ById<IntegerEditor>("IdHayvanRef"); } }
        public IntegerEditor IdYemRef { get { return ById<IntegerEditor>("IdYemRef"); } }
        public DateEditor BeslemeTarihi { get { return ById<DateEditor>("BeslemeTarihi"); } }
    }
}