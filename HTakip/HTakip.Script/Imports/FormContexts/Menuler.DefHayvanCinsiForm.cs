﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public partial class DefHayvanCinsiForm : PrefixedContext
    {
        [InlineConstant] public const string FormKey = "Menuler.DefHayvanCinsi";

        public DefHayvanCinsiForm(string idPrefix) : base(idPrefix) {}


        public IntegerEditor IdHayvanTuruRef { get { return ById<IntegerEditor>("IdHayvanTuruRef"); } }
        public StringEditor HayvanCinsiAdi { get { return ById<StringEditor>("HayvanCinsiAdi"); } }
    }
}