﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public partial class DefYemForm : PrefixedContext
    {
        [InlineConstant] public const string FormKey = "Menuler.DefYem";

        public DefYemForm(string idPrefix) : base(idPrefix) {}


        public StringEditor YemAdi { get { return ById<StringEditor>("YemAdi"); } }
    }
}