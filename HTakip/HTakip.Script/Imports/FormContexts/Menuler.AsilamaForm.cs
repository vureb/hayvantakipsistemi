﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public partial class AsilamaForm : PrefixedContext
    {
        [InlineConstant] public const string FormKey = "Menuler.Asilama";

        public AsilamaForm(string idPrefix) : base(idPrefix) {}


        public IntegerEditor IdHayvanRef { get { return ById<IntegerEditor>("IdHayvanRef"); } }
        public DateEditor AsilamaTarihi { get { return ById<DateEditor>("AsilamaTarihi"); } }
        public IntegerEditor IdGorevliRef { get { return ById<IntegerEditor>("IdGorevliRef"); } }
        public BooleanEditor AsilamaYapildimi { get { return ById<BooleanEditor>("AsilamaYapildimi"); } }
        public IntegerEditor IdAsiRef { get { return ById<IntegerEditor>("IdAsiRef"); } }
    }
}