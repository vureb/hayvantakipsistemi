﻿
namespace HTakip.Menuler
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public partial class DefHayvanTurleriForm : PrefixedContext
    {
        [InlineConstant] public const string FormKey = "Menuler.DefHayvanTurleri";

        public DefHayvanTurleriForm(string idPrefix) : base(idPrefix) {}


        public StringEditor HayvanTuruAdi { get { return ById<StringEditor>("HayvanTuruAdi"); } }
    }
}