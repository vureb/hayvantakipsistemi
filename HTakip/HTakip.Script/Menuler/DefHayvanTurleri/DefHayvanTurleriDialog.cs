﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System.Collections.Generic;

    [IdProperty(DefHayvanTurleriRow.IdProperty), NameProperty(DefHayvanTurleriRow.NameProperty)]
    [FormKey("Menuler.DefHayvanTurleri"), LocalTextPrefix(DefHayvanTurleriRow.LocalTextPrefix), Service(DefHayvanTurleriService.BaseUrl)]
    public class DefHayvanTurleriDialog : EntityDialog<DefHayvanTurleriRow>
    {
        protected DefHayvanTurleriForm form;

        public DefHayvanTurleriDialog()
        {
            form = new DefHayvanTurleriForm(this.IdPrefix);
        }
    }
}