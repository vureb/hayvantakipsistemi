﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [ColumnsKey("Menuler.DefHayvanTurleri"), IdProperty(DefHayvanTurleriRow.IdProperty), NameProperty(DefHayvanTurleriRow.NameProperty)]
    [DialogType(typeof(DefHayvanTurleriDialog)), LocalTextPrefix(DefHayvanTurleriRow.LocalTextPrefix), Service(DefHayvanTurleriService.BaseUrl)]
    public class DefHayvanTurleriGrid : EntityGrid<DefHayvanTurleriRow>
    {
        public DefHayvanTurleriGrid(jQueryObject container)
            : base(container)
        {
        }
    }
}