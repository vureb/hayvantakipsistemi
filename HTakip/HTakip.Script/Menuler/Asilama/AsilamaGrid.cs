﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [ColumnsKey("Menuler.Asilama"), IdProperty(AsilamaRow.IdProperty)]
    [DialogType(typeof(AsilamaDialog)), LocalTextPrefix(AsilamaRow.LocalTextPrefix), Service(AsilamaService.BaseUrl)]
    public class AsilamaGrid : EntityGrid<AsilamaRow>
    {
        public AsilamaGrid(jQueryObject container)
            : base(container)
        {
        }
    }
}