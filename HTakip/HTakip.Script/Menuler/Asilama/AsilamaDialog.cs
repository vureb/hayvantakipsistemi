﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System.Collections.Generic;

    [IdProperty(AsilamaRow.IdProperty)]
    [FormKey("Menuler.Asilama"), LocalTextPrefix(AsilamaRow.LocalTextPrefix), Service(AsilamaService.BaseUrl)]
    public class AsilamaDialog : EntityDialog<AsilamaRow>
    {
        protected AsilamaForm form;

        public AsilamaDialog()
        {
            form = new AsilamaForm(this.IdPrefix);
        }
    }
}