﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [ColumnsKey("Menuler.Hayvan"), IdProperty(HayvanRow.IdProperty), NameProperty(HayvanRow.NameProperty)]
    [DialogType(typeof(HayvanDialog)), LocalTextPrefix(HayvanRow.LocalTextPrefix), Service(HayvanService.BaseUrl)]
    public class HayvanGrid : EntityGrid<HayvanRow>
    {
        public HayvanGrid(jQueryObject container)
            : base(container)
        {
        }
    }
}