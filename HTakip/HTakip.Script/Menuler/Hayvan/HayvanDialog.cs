﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System.Collections.Generic;

    [IdProperty(HayvanRow.IdProperty), NameProperty(HayvanRow.NameProperty)]
    [FormKey("Menuler.Hayvan"), LocalTextPrefix(HayvanRow.LocalTextPrefix), Service(HayvanService.BaseUrl)]
    public class HayvanDialog : EntityDialog<HayvanRow>
    {
        protected HayvanForm form;

        public HayvanDialog()
        {
            form = new HayvanForm(this.IdPrefix);
        }
    }
}