﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [ColumnsKey("Menuler.HayvanTedavi"), IdProperty(HayvanTedaviRow.IdProperty), NameProperty(HayvanTedaviRow.NameProperty)]
    [DialogType(typeof(HayvanTedaviDialog)), LocalTextPrefix(HayvanTedaviRow.LocalTextPrefix), Service(HayvanTedaviService.BaseUrl)]
    public class HayvanTedaviGrid : EntityGrid<HayvanTedaviRow>
    {
        public HayvanTedaviGrid(jQueryObject container)
            : base(container)
        {
        }
    }
}