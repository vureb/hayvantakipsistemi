﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System.Collections.Generic;

    [IdProperty(HayvanTedaviRow.IdProperty), NameProperty(HayvanTedaviRow.NameProperty)]
    [FormKey("Menuler.HayvanTedavi"), LocalTextPrefix(HayvanTedaviRow.LocalTextPrefix), Service(HayvanTedaviService.BaseUrl)]
    public class HayvanTedaviDialog : EntityDialog<HayvanTedaviRow>
    {
        protected HayvanTedaviForm form;

        public HayvanTedaviDialog()
        {
            form = new HayvanTedaviForm(this.IdPrefix);
        }
    }
}