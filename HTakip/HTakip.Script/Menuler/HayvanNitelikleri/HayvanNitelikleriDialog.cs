﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System.Collections.Generic;

    [IdProperty(HayvanNitelikleriRow.IdProperty)]
    [FormKey("Menuler.HayvanNitelikleri"), LocalTextPrefix(HayvanNitelikleriRow.LocalTextPrefix), Service(HayvanNitelikleriService.BaseUrl)]
    public class HayvanNitelikleriDialog : EntityDialog<HayvanNitelikleriRow>
    {
        protected HayvanNitelikleriForm form;

        public HayvanNitelikleriDialog()
        {
            form = new HayvanNitelikleriForm(this.IdPrefix);
        }
    }
}