﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [ColumnsKey("Menuler.HayvanNitelikleri"), IdProperty(HayvanNitelikleriRow.IdProperty)]
    [DialogType(typeof(HayvanNitelikleriDialog)), LocalTextPrefix(HayvanNitelikleriRow.LocalTextPrefix), Service(HayvanNitelikleriService.BaseUrl)]
    public class HayvanNitelikleriGrid : EntityGrid<HayvanNitelikleriRow>
    {
        public HayvanNitelikleriGrid(jQueryObject container)
            : base(container)
        {
        }
    }
}