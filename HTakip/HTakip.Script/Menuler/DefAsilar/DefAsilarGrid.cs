﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [ColumnsKey("Menuler.DefAsilar"), IdProperty(DefAsilarRow.IdProperty), NameProperty(DefAsilarRow.NameProperty)]
    [DialogType(typeof(DefAsilarDialog)), LocalTextPrefix(DefAsilarRow.LocalTextPrefix), Service(DefAsilarService.BaseUrl)]
    public class DefAsilarGrid : EntityGrid<DefAsilarRow>
    {
        public DefAsilarGrid(jQueryObject container)
            : base(container)
        {
        }
    }
}