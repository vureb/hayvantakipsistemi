﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System.Collections.Generic;

    [IdProperty(DefAsilarRow.IdProperty), NameProperty(DefAsilarRow.NameProperty)]
    [FormKey("Menuler.DefAsilar"), LocalTextPrefix(DefAsilarRow.LocalTextPrefix), Service(DefAsilarService.BaseUrl)]
    public class DefAsilarDialog : EntityDialog<DefAsilarRow>
    {
        protected DefAsilarForm form;

        public DefAsilarDialog()
        {
            form = new DefAsilarForm(this.IdPrefix);
        }
    }
}