﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System.Collections.Generic;

    [IdProperty(GorevliRow.IdProperty), NameProperty(GorevliRow.NameProperty)]
    [FormKey("Menuler.Gorevli"), LocalTextPrefix(GorevliRow.LocalTextPrefix), Service(GorevliService.BaseUrl)]
    public class GorevliDialog : EntityDialog<GorevliRow>
    {
        protected GorevliForm form;

        public GorevliDialog()
        {
            form = new GorevliForm(this.IdPrefix);
        }
    }
}