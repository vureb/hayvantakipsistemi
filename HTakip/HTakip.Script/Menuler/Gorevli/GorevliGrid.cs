﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [ColumnsKey("Menuler.Gorevli"), IdProperty(GorevliRow.IdProperty), NameProperty(GorevliRow.NameProperty)]
    [DialogType(typeof(GorevliDialog)), LocalTextPrefix(GorevliRow.LocalTextPrefix), Service(GorevliService.BaseUrl)]
    public class GorevliGrid : EntityGrid<GorevliRow>
    {
        public GorevliGrid(jQueryObject container)
            : base(container)
        {
        }
    }
}