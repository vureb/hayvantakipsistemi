﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System.Collections.Generic;

    [IdProperty(DefHayvanCinsiRow.IdProperty), NameProperty(DefHayvanCinsiRow.NameProperty)]
    [FormKey("Menuler.DefHayvanCinsi"), LocalTextPrefix(DefHayvanCinsiRow.LocalTextPrefix), Service(DefHayvanCinsiService.BaseUrl)]
    public class DefHayvanCinsiDialog : EntityDialog<DefHayvanCinsiRow>
    {
        protected DefHayvanCinsiForm form;

        public DefHayvanCinsiDialog()
        {
            form = new DefHayvanCinsiForm(this.IdPrefix);
        }
    }
}