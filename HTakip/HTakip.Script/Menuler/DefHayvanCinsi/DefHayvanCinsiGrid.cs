﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [ColumnsKey("Menuler.DefHayvanCinsi"), IdProperty(DefHayvanCinsiRow.IdProperty), NameProperty(DefHayvanCinsiRow.NameProperty)]
    [DialogType(typeof(DefHayvanCinsiDialog)), LocalTextPrefix(DefHayvanCinsiRow.LocalTextPrefix), Service(DefHayvanCinsiService.BaseUrl)]
    public class DefHayvanCinsiGrid : EntityGrid<DefHayvanCinsiRow>
    {
        public DefHayvanCinsiGrid(jQueryObject container)
            : base(container)
        {
        }
    }
}