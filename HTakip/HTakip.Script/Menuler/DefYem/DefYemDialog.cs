﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System.Collections.Generic;

    [IdProperty(DefYemRow.IdProperty), NameProperty(DefYemRow.NameProperty)]
    [FormKey("Menuler.DefYem"), LocalTextPrefix(DefYemRow.LocalTextPrefix), Service(DefYemService.BaseUrl)]
    public class DefYemDialog : EntityDialog<DefYemRow>
    {
        protected DefYemForm form;

        public DefYemDialog()
        {
            form = new DefYemForm(this.IdPrefix);
        }
    }
}