﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [ColumnsKey("Menuler.DefYem"), IdProperty(DefYemRow.IdProperty), NameProperty(DefYemRow.NameProperty)]
    [DialogType(typeof(DefYemDialog)), LocalTextPrefix(DefYemRow.LocalTextPrefix), Service(DefYemService.BaseUrl)]
    public class DefYemGrid : EntityGrid<DefYemRow>
    {
        public DefYemGrid(jQueryObject container)
            : base(container)
        {
        }
    }
}