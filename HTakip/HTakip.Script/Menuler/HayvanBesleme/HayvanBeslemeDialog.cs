﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System.Collections.Generic;

    [IdProperty(HayvanBeslemeRow.IdProperty)]
    [FormKey("Menuler.HayvanBesleme"), LocalTextPrefix(HayvanBeslemeRow.LocalTextPrefix), Service(HayvanBeslemeService.BaseUrl)]
    public class HayvanBeslemeDialog : EntityDialog<HayvanBeslemeRow>
    {
        protected HayvanBeslemeForm form;

        public HayvanBeslemeDialog()
        {
            form = new HayvanBeslemeForm(this.IdPrefix);
        }
    }
}