﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [ColumnsKey("Menuler.HayvanBesleme"), IdProperty(HayvanBeslemeRow.IdProperty)]
    [DialogType(typeof(HayvanBeslemeDialog)), LocalTextPrefix(HayvanBeslemeRow.LocalTextPrefix), Service(HayvanBeslemeService.BaseUrl)]
    public class HayvanBeslemeGrid : EntityGrid<HayvanBeslemeRow>
    {
        public HayvanBeslemeGrid(jQueryObject container)
            : base(container)
        {
        }
    }
}