﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [ColumnsKey("Menuler.DefHastalik"), IdProperty(DefHastalikRow.IdProperty), NameProperty(DefHastalikRow.NameProperty)]
    [DialogType(typeof(DefHastalikDialog)), LocalTextPrefix(DefHastalikRow.LocalTextPrefix), Service(DefHastalikService.BaseUrl)]
    public class DefHastalikGrid : EntityGrid<DefHastalikRow>
    {
        public DefHastalikGrid(jQueryObject container)
            : base(container)
        {
        }
    }
}