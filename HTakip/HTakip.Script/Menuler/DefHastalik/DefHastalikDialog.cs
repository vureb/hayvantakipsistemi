﻿
namespace HTakip.Menuler
{
    using jQueryApi;
    using Serenity;
    using System.Collections.Generic;

    [IdProperty(DefHastalikRow.IdProperty), NameProperty(DefHastalikRow.NameProperty)]
    [FormKey("Menuler.DefHastalik"), LocalTextPrefix(DefHastalikRow.LocalTextPrefix), Service(DefHastalikService.BaseUrl)]
    public class DefHastalikDialog : EntityDialog<DefHastalikRow>
    {
        protected DefHastalikForm form;

        public DefHastalikDialog()
        {
            form = new DefHastalikForm(this.IdPrefix);
        }
    }
}