﻿
namespace HTakip.Menuler.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Menuler.HayvanNitelikleri")]
    [BasedOnRow(typeof(Entities.HayvanNitelikleriRow))]
    public class HayvanNitelikleriForm
    {
        public Int32 IdHayvanRef { get; set; }
        public DateTime OlcumTarihi { get; set; }
        public Double Agirlik { get; set; }
        public Double Boyu { get; set; }
    }
}