﻿
namespace HTakip.Menuler.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Menuler.HayvanNitelikleri")]
    [BasedOnRow(typeof(Entities.HayvanNitelikleriRow))]
    public class HayvanNitelikleriColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 IdNitelik { get; set; }
        public Int32 IdHayvanRef { get; set; }
        public DateTime OlcumTarihi { get; set; }
        public Double Agirlik { get; set; }
        public Double Boyu { get; set; }
    }
}