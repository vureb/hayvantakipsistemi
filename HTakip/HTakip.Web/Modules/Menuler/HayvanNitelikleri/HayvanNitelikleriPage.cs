﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Menuler/HayvanNitelikleri", typeof(HTakip.Menuler.Pages.HayvanNitelikleriController))]

namespace HTakip.Menuler.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Menuler/HayvanNitelikleri"), Route("{action=index}")]
    public class HayvanNitelikleriController : Controller
    {
        [PageAuthorize("Administration")]
        public ActionResult Index()
        {
            return View("~/Modules/Menuler/HayvanNitelikleri/HayvanNitelikleriIndex.cshtml");
        }
    }
}