﻿
namespace HTakip.Menuler.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Northwind"), DisplayName("HayvanNitelikleri"), InstanceName("HayvanNitelikleri"), TwoLevelCached]
    [ReadPermission("Administration")]
    [ModifyPermission("Administration")]
    public sealed class HayvanNitelikleriRow : Row, IIdRow
    {
        [DisplayName("Id Nitelik"), PrimaryKey]
        public Int32? IdNitelik
        {
            get { return Fields.IdNitelik[this]; }
            set { Fields.IdNitelik[this] = value; }
        }

        [DisplayName("Id Hayvan Ref"), NotNull, ForeignKey("[dbo].[Hayvan]", "IdHayvan"), LeftJoin("jIdHayvanRef"), TextualField("IdHayvanRefKupeNo")]
        public Int32? IdHayvanRef
        {
            get { return Fields.IdHayvanRef[this]; }
            set { Fields.IdHayvanRef[this] = value; }
        }

        [DisplayName("Olcum Tarihi")]
        public DateTime? OlcumTarihi
        {
            get { return Fields.OlcumTarihi[this]; }
            set { Fields.OlcumTarihi[this] = value; }
        }

        [DisplayName("Agirlik"), NotNull]
        public Double? Agirlik
        {
            get { return Fields.Agirlik[this]; }
            set { Fields.Agirlik[this] = value; }
        }

        [DisplayName("Boyu"), NotNull]
        public Double? Boyu
        {
            get { return Fields.Boyu[this]; }
            set { Fields.Boyu[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Kupe No"), Expression("jIdHayvanRef.[KupeNo]")]
        public String IdHayvanRefKupeNo
        {
            get { return Fields.IdHayvanRefKupeNo[this]; }
            set { Fields.IdHayvanRefKupeNo[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Cinsiyet"), Expression("jIdHayvanRef.[Cinsiyet]")]
        public Int32? IdHayvanRefCinsiyet
        {
            get { return Fields.IdHayvanRefCinsiyet[this]; }
            set { Fields.IdHayvanRefCinsiyet[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Id Hayvan Turu Ref"), Expression("jIdHayvanRef.[IdHayvanTuruRef]")]
        public Int32? IdHayvanRefIdHayvanTuruRef
        {
            get { return Fields.IdHayvanRefIdHayvanTuruRef[this]; }
            set { Fields.IdHayvanRefIdHayvanTuruRef[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Id Hayvan Cinsi Ref"), Expression("jIdHayvanRef.[IdHayvanCinsiRef]")]
        public Int32? IdHayvanRefIdHayvanCinsiRef
        {
            get { return Fields.IdHayvanRefIdHayvanCinsiRef[this]; }
            set { Fields.IdHayvanRefIdHayvanCinsiRef[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Satildi Durumu"), Expression("jIdHayvanRef.[SatildiDurumu]")]
        public Boolean? IdHayvanRefSatildiDurumu
        {
            get { return Fields.IdHayvanRefSatildiDurumu[this]; }
            set { Fields.IdHayvanRefSatildiDurumu[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Kesildi Durumu"), Expression("jIdHayvanRef.[KesildiDurumu]")]
        public Boolean? IdHayvanRefKesildiDurumu
        {
            get { return Fields.IdHayvanRefKesildiDurumu[this]; }
            set { Fields.IdHayvanRefKesildiDurumu[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Telef Durumu"), Expression("jIdHayvanRef.[TelefDurumu]")]
        public Boolean? IdHayvanRefTelefDurumu
        {
            get { return Fields.IdHayvanRefTelefDurumu[this]; }
            set { Fields.IdHayvanRefTelefDurumu[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Aciklama"), Expression("jIdHayvanRef.[Aciklama]")]
        public String IdHayvanRefAciklama
        {
            get { return Fields.IdHayvanRefAciklama[this]; }
            set { Fields.IdHayvanRefAciklama[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Hayvan Dogum Tarihi"), Expression("jIdHayvanRef.[HayvanDogumTarihi]")]
        public DateTime? IdHayvanRefHayvanDogumTarihi
        {
            get { return Fields.IdHayvanRefHayvanDogumTarihi[this]; }
            set { Fields.IdHayvanRefHayvanDogumTarihi[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Padok No"), Expression("jIdHayvanRef.[PadokNo]")]
        public Int32? IdHayvanRefPadokNo
        {
            get { return Fields.IdHayvanRefPadokNo[this]; }
            set { Fields.IdHayvanRefPadokNo[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.IdNitelik; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public HayvanNitelikleriRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field IdNitelik;
            public Int32Field IdHayvanRef;
            public DateTimeField OlcumTarihi;
            public DoubleField Agirlik;
            public DoubleField Boyu;

            public StringField IdHayvanRefKupeNo;
            public Int32Field IdHayvanRefCinsiyet;
            public Int32Field IdHayvanRefIdHayvanTuruRef;
            public Int32Field IdHayvanRefIdHayvanCinsiRef;
            public BooleanField IdHayvanRefSatildiDurumu;
            public BooleanField IdHayvanRefKesildiDurumu;
            public BooleanField IdHayvanRefTelefDurumu;
            public StringField IdHayvanRefAciklama;
            public DateTimeField IdHayvanRefHayvanDogumTarihi;
            public Int32Field IdHayvanRefPadokNo;

            public RowFields()
                : base("[dbo].[HayvanNitelikleri]")
            {
                LocalTextPrefix = "Menuler.HayvanNitelikleri";
            }
        }
    }
}