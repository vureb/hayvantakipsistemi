﻿
namespace HTakip.Menuler.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Northwind"), DisplayName("Hayvan"), InstanceName("Hayvan"), TwoLevelCached]
    [ReadPermission("Administration")]
    [ModifyPermission("Administration")]
    public sealed class HayvanRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id Hayvan"), PrimaryKey]
        public Int32? IdHayvan
        {
            get { return Fields.IdHayvan[this]; }
            set { Fields.IdHayvan[this] = value; }
        }

        [DisplayName("Kupe No"), Size(50), NotNull, QuickSearch]
        public String KupeNo
        {
            get { return Fields.KupeNo[this]; }
            set { Fields.KupeNo[this] = value; }
        }

        [DisplayName("Cinsiyet")]
        public Int32? Cinsiyet
        {
            get { return Fields.Cinsiyet[this]; }
            set { Fields.Cinsiyet[this] = value; }
        }

        [DisplayName("Id Hayvan Turu Ref")]
        public Int32? IdHayvanTuruRef
        {
            get { return Fields.IdHayvanTuruRef[this]; }
            set { Fields.IdHayvanTuruRef[this] = value; }
        }

        [DisplayName("Id Hayvan Cinsi Ref"), ForeignKey("[dbo].[Def_HayvanCinsi]", "IdHayvanCinsi"), LeftJoin("jIdHayvanCinsiRef"), TextualField("IdHayvanCinsiRefHayvanCinsiAdi")]
        public Int32? IdHayvanCinsiRef
        {
            get { return Fields.IdHayvanCinsiRef[this]; }
            set { Fields.IdHayvanCinsiRef[this] = value; }
        }

        [DisplayName("Satildi Durumu")]
        public Boolean? SatildiDurumu
        {
            get { return Fields.SatildiDurumu[this]; }
            set { Fields.SatildiDurumu[this] = value; }
        }

        [DisplayName("Kesildi Durumu")]
        public Boolean? KesildiDurumu
        {
            get { return Fields.KesildiDurumu[this]; }
            set { Fields.KesildiDurumu[this] = value; }
        }

        [DisplayName("Telef Durumu")]
        public Boolean? TelefDurumu
        {
            get { return Fields.TelefDurumu[this]; }
            set { Fields.TelefDurumu[this] = value; }
        }

        [DisplayName("Aciklama")]
        public String Aciklama
        {
            get { return Fields.Aciklama[this]; }
            set { Fields.Aciklama[this] = value; }
        }

        [DisplayName("Hayvan Dogum Tarihi")]
        public DateTime? HayvanDogumTarihi
        {
            get { return Fields.HayvanDogumTarihi[this]; }
            set { Fields.HayvanDogumTarihi[this] = value; }
        }

        [DisplayName("Padok No")]
        public Int32? PadokNo
        {
            get { return Fields.PadokNo[this]; }
            set { Fields.PadokNo[this] = value; }
        }

        [DisplayName("Süt Miktarı")]
        public Int32? SutMiktari
        {
            get { return Fields.SutMiktari[this]; }
            set { Fields.SutMiktari[this] = value; }
        }


        [DisplayName("Id Hayvan Cinsi Ref Id Hayvan Turu Ref"), Expression("jIdHayvanCinsiRef.[IdHayvanTuruRef]")]
        public Int32? IdHayvanCinsiRefIdHayvanTuruRef
        {
            get { return Fields.IdHayvanCinsiRefIdHayvanTuruRef[this]; }
            set { Fields.IdHayvanCinsiRefIdHayvanTuruRef[this] = value; }
        }

        [DisplayName("Id Hayvan Cinsi Ref Hayvan Cinsi Adi"), Expression("jIdHayvanCinsiRef.[HayvanCinsiAdi]")]
        public String IdHayvanCinsiRefHayvanCinsiAdi
        {
            get { return Fields.IdHayvanCinsiRefHayvanCinsiAdi[this]; }
            set { Fields.IdHayvanCinsiRefHayvanCinsiAdi[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.IdHayvan; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.KupeNo; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public HayvanRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field IdHayvan;
            public StringField KupeNo;
            public Int32Field Cinsiyet;
            public Int32Field IdHayvanTuruRef;
            public Int32Field IdHayvanCinsiRef;
            public BooleanField SatildiDurumu;
            public BooleanField KesildiDurumu;
            public BooleanField TelefDurumu;
            public StringField Aciklama;
            public DateTimeField HayvanDogumTarihi;
            public Int32Field PadokNo;
            public Int32Field SutMiktari;
            public Int32Field IdHayvanCinsiRefIdHayvanTuruRef;
            public StringField IdHayvanCinsiRefHayvanCinsiAdi;

            public RowFields()
                : base("[dbo].[Hayvan]")
            {
                LocalTextPrefix = "Menuler.Hayvan";
            }
        }
    }
}