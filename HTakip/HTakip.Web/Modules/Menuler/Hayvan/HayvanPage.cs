﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Menuler/Hayvan", typeof(HTakip.Menuler.Pages.HayvanController))]

namespace HTakip.Menuler.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Menuler/Hayvan"), Route("{action=index}")]
    public class HayvanController : Controller
    {
        [PageAuthorize("Administration")]
        public ActionResult Index()
        {
            return View("~/Modules/Menuler/Hayvan/HayvanIndex.cshtml");
        }
    }
}