﻿
namespace HTakip.Menuler.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Menuler.Hayvan")]
    [BasedOnRow(typeof(Entities.HayvanRow))]
    public class HayvanColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 IdHayvan { get; set; }
        [EditLink]
        public String KupeNo { get; set; }
        public Int32 Cinsiyet { get; set; }
        public Int32 IdHayvanTuruRef { get; set; }
        public Int32 IdHayvanCinsiRef { get; set; }
        public Boolean SatildiDurumu { get; set; }
        public Boolean KesildiDurumu { get; set; }
        public Boolean TelefDurumu { get; set; }
        public String Aciklama { get; set; }
        public DateTime HayvanDogumTarihi { get; set; }
        public Int32 PadokNo { get; set; }
        public Int32 SutMiktari { get; set; }
    }
}