﻿
namespace HTakip.Menuler.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Northwind"), DisplayName("HayvanBesleme"), InstanceName("HayvanBesleme"), TwoLevelCached]
    [ReadPermission("Administration")]
    [ModifyPermission("Administration")]
    public sealed class HayvanBeslemeRow : Row, IIdRow
    {
        [DisplayName("Id Besleme"), PrimaryKey]
        public Int32? IdBesleme
        {
            get { return Fields.IdBesleme[this]; }
            set { Fields.IdBesleme[this] = value; }
        }

        [DisplayName("Id Hayvan Ref"), ForeignKey("[dbo].[Def_Yem]", "IdYem"), LeftJoin("jIdHayvanRef"), TextualField("IdHayvanRefYemAdi")]
        public Int32? IdHayvanRef
        {
            get { return Fields.IdHayvanRef[this]; }
            set { Fields.IdHayvanRef[this] = value; }
        }

        [DisplayName("Id Yem Ref")]
        public Int32? IdYemRef
        {
            get { return Fields.IdYemRef[this]; }
            set { Fields.IdYemRef[this] = value; }
        }

        [DisplayName("Besleme Tarihi")]
        public DateTime? BeslemeTarihi
        {
            get { return Fields.BeslemeTarihi[this]; }
            set { Fields.BeslemeTarihi[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Yem Adi"), Expression("jIdHayvanRef.[YemAdi]")]
        public String IdHayvanRefYemAdi
        {
            get { return Fields.IdHayvanRefYemAdi[this]; }
            set { Fields.IdHayvanRefYemAdi[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.IdBesleme; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public HayvanBeslemeRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field IdBesleme;
            public Int32Field IdHayvanRef;
            public Int32Field IdYemRef;
            public DateTimeField BeslemeTarihi;

            public StringField IdHayvanRefYemAdi;

            public RowFields()
                : base("[dbo].[HayvanBesleme]")
            {
                LocalTextPrefix = "Menuler.HayvanBesleme";
            }
        }
    }
}