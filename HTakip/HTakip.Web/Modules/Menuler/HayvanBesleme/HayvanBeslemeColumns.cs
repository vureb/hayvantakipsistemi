﻿
namespace HTakip.Menuler.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Menuler.HayvanBesleme")]
    [BasedOnRow(typeof(Entities.HayvanBeslemeRow))]
    public class HayvanBeslemeColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 IdBesleme { get; set; }
        public Int32 IdHayvanRef { get; set; }
        public Int32 IdYemRef { get; set; }
        public DateTime BeslemeTarihi { get; set; }
    }
}