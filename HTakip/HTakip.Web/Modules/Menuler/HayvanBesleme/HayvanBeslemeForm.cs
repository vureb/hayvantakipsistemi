﻿
namespace HTakip.Menuler.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Menuler.HayvanBesleme")]
    [BasedOnRow(typeof(Entities.HayvanBeslemeRow))]
    public class HayvanBeslemeForm
    {
        public Int32 IdHayvanRef { get; set; }
        public Int32 IdYemRef { get; set; }
        public DateTime BeslemeTarihi { get; set; }
    }
}