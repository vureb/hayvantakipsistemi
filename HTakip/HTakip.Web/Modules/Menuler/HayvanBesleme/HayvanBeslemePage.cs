﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Menuler/HayvanBesleme", typeof(HTakip.Menuler.Pages.HayvanBeslemeController))]

namespace HTakip.Menuler.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Menuler/HayvanBesleme"), Route("{action=index}")]
    public class HayvanBeslemeController : Controller
    {
        [PageAuthorize("Administration")]
        public ActionResult Index()
        {
            return View("~/Modules/Menuler/HayvanBesleme/HayvanBeslemeIndex.cshtml");
        }
    }
}