﻿
namespace HTakip.Menuler.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Menuler.Gorevli")]
    [BasedOnRow(typeof(Entities.GorevliRow))]
    public class GorevliColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 IdGorevli { get; set; }
        public Int32 Yetki { get; set; }
        public String Tc { get; set; }
        [EditLink]
        public String Ad { get; set; }
        public String Soyad { get; set; }
        public String Adres { get; set; }
        public String Telefon { get; set; }
        public String Mail { get; set; }
        public Boolean IstenCiktiMi { get; set; }
    }
}