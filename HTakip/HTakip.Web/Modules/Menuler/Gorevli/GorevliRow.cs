﻿
namespace HTakip.Menuler.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Northwind"), DisplayName("Gorevli"), InstanceName("Gorevli"), TwoLevelCached]
    [ReadPermission("Administration")]
    [ModifyPermission("Administration")]
    public sealed class GorevliRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id Gorevli"), PrimaryKey]
        public Int32? IdGorevli
        {
            get { return Fields.IdGorevli[this]; }
            set { Fields.IdGorevli[this] = value; }
        }

        [DisplayName("Yetki"), NotNull]
        public Int32? Yetki
        {
            get { return Fields.Yetki[this]; }
            set { Fields.Yetki[this] = value; }
        }

        [DisplayName("Tc"), Column("TC"), NotNull]
        public String Tc
        {
            get { return Fields.Tc[this]; }
            set { Fields.Tc[this] = value; }
        }

        [DisplayName("Ad"), Size(50), NotNull, QuickSearch]
        public String Ad
        {
            get { return Fields.Ad[this]; }
            set { Fields.Ad[this] = value; }
        }

        [DisplayName("Soyad"), Size(50), NotNull]
        public String Soyad
        {
            get { return Fields.Soyad[this]; }
            set { Fields.Soyad[this] = value; }
        }

        [DisplayName("Adres")]
        public String Adres
        {
            get { return Fields.Adres[this]; }
            set { Fields.Adres[this] = value; }
        }

        [DisplayName("Telefon")]
        public String Telefon
        {
            get { return Fields.Telefon[this]; }
            set { Fields.Telefon[this] = value; }
        }

        [DisplayName("Mail"), Size(50)]
        public String Mail
        {
            get { return Fields.Mail[this]; }
            set { Fields.Mail[this] = value; }
        }

        [DisplayName("Isten Cikti Mi")]
        public Boolean? IstenCiktiMi
        {
            get { return Fields.IstenCiktiMi[this]; }
            set { Fields.IstenCiktiMi[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.IdGorevli; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Ad; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public GorevliRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field IdGorevli;
            public Int32Field Yetki;
            public StringField Tc;
            public StringField Ad;
            public StringField Soyad;
            public StringField Adres;
            public StringField Telefon;
            public StringField Mail;
            public BooleanField IstenCiktiMi;

            public RowFields()
                : base("[dbo].[Gorevli]")
            {
                LocalTextPrefix = "Menuler.Gorevli";
            }
        }
    }
}