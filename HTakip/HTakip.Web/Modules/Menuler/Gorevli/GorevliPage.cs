﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Menuler/Gorevli", typeof(HTakip.Menuler.Pages.GorevliController))]

namespace HTakip.Menuler.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Menuler/Gorevli"), Route("{action=index}")]
    public class GorevliController : Controller
    {
        [PageAuthorize("Administration")]
        public ActionResult Index()
        {
            return View("~/Modules/Menuler/Gorevli/GorevliIndex.cshtml");
        }
    }
}