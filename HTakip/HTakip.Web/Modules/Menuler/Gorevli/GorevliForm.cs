﻿
namespace HTakip.Menuler.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Menuler.Gorevli")]
    [BasedOnRow(typeof(Entities.GorevliRow))]
    public class GorevliForm
    {
        public Int32 Yetki { get; set; }
        public String Tc { get; set; }
        public String Ad { get; set; }
        public String Soyad { get; set; }
        public String Adres { get; set; }
        public Int32 Telefon { get; set; }
        public String Mail { get; set; }
        public Boolean IstenCiktiMi { get; set; }
    }
}