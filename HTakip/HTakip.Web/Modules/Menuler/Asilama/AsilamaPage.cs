﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Menuler/Asilama", typeof(HTakip.Menuler.Pages.AsilamaController))]

namespace HTakip.Menuler.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Menuler/Asilama"), Route("{action=index}")]
    public class AsilamaController : Controller
    {
        [PageAuthorize("Administration")]
        public ActionResult Index()
        {
            return View("~/Modules/Menuler/Asilama/AsilamaIndex.cshtml");
        }
    }
}