﻿
namespace HTakip.Menuler.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Northwind"), DisplayName("Asilama"), InstanceName("Asilama"), TwoLevelCached]
    [ReadPermission("Administration")]
    [ModifyPermission("Administration")]
    public sealed class AsilamaRow : Row, IIdRow
    {
        [DisplayName("Id Asilama"), PrimaryKey]
        public Int32? IdAsilama
        {
            get { return Fields.IdAsilama[this]; }
            set { Fields.IdAsilama[this] = value; }
        }

        [DisplayName("Id Hayvan Ref"), ForeignKey("[dbo].[Hayvan]", "IdHayvan"), LeftJoin("jIdHayvanRef"), TextualField("IdHayvanRefKupeNo")]
        public Int32? IdHayvanRef
        {
            get { return Fields.IdHayvanRef[this]; }
            set { Fields.IdHayvanRef[this] = value; }
        }

        [DisplayName("Asilama Tarihi")]
        public DateTime? AsilamaTarihi
        {
            get { return Fields.AsilamaTarihi[this]; }
            set { Fields.AsilamaTarihi[this] = value; }
        }

        [DisplayName("Id Gorevli Ref"), ForeignKey("[dbo].[Gorevli]", "IdGorevli"), LeftJoin("jIdGorevliRef"), TextualField("IdGorevliRefAd")]
        public Int32? IdGorevliRef
        {
            get { return Fields.IdGorevliRef[this]; }
            set { Fields.IdGorevliRef[this] = value; }
        }

        [DisplayName("Asilama Yapildimi"), NotNull]
        public Boolean? AsilamaYapildimi
        {
            get { return Fields.AsilamaYapildimi[this]; }
            set { Fields.AsilamaYapildimi[this] = value; }
        }

        [DisplayName("Id Asi Ref"), ForeignKey("[dbo].[Def_Asilar]", "IdAsi"), LeftJoin("jIdAsiRef"), TextualField("IdAsiRefAsiAdi")]
        public Int32? IdAsiRef
        {
            get { return Fields.IdAsiRef[this]; }
            set { Fields.IdAsiRef[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Kupe No"), Expression("jIdHayvanRef.[KupeNo]")]
        public String IdHayvanRefKupeNo
        {
            get { return Fields.IdHayvanRefKupeNo[this]; }
            set { Fields.IdHayvanRefKupeNo[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Cinsiyet"), Expression("jIdHayvanRef.[Cinsiyet]")]
        public Int32? IdHayvanRefCinsiyet
        {
            get { return Fields.IdHayvanRefCinsiyet[this]; }
            set { Fields.IdHayvanRefCinsiyet[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Id Hayvan Turu Ref"), Expression("jIdHayvanRef.[IdHayvanTuruRef]")]
        public Int32? IdHayvanRefIdHayvanTuruRef
        {
            get { return Fields.IdHayvanRefIdHayvanTuruRef[this]; }
            set { Fields.IdHayvanRefIdHayvanTuruRef[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Id Hayvan Cinsi Ref"), Expression("jIdHayvanRef.[IdHayvanCinsiRef]")]
        public Int32? IdHayvanRefIdHayvanCinsiRef
        {
            get { return Fields.IdHayvanRefIdHayvanCinsiRef[this]; }
            set { Fields.IdHayvanRefIdHayvanCinsiRef[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Satildi Durumu"), Expression("jIdHayvanRef.[SatildiDurumu]")]
        public Boolean? IdHayvanRefSatildiDurumu
        {
            get { return Fields.IdHayvanRefSatildiDurumu[this]; }
            set { Fields.IdHayvanRefSatildiDurumu[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Kesildi Durumu"), Expression("jIdHayvanRef.[KesildiDurumu]")]
        public Boolean? IdHayvanRefKesildiDurumu
        {
            get { return Fields.IdHayvanRefKesildiDurumu[this]; }
            set { Fields.IdHayvanRefKesildiDurumu[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Telef Durumu"), Expression("jIdHayvanRef.[TelefDurumu]")]
        public Boolean? IdHayvanRefTelefDurumu
        {
            get { return Fields.IdHayvanRefTelefDurumu[this]; }
            set { Fields.IdHayvanRefTelefDurumu[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Aciklama"), Expression("jIdHayvanRef.[Aciklama]")]
        public String IdHayvanRefAciklama
        {
            get { return Fields.IdHayvanRefAciklama[this]; }
            set { Fields.IdHayvanRefAciklama[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Hayvan Dogum Tarihi"), Expression("jIdHayvanRef.[HayvanDogumTarihi]")]
        public DateTime? IdHayvanRefHayvanDogumTarihi
        {
            get { return Fields.IdHayvanRefHayvanDogumTarihi[this]; }
            set { Fields.IdHayvanRefHayvanDogumTarihi[this] = value; }
        }

        [DisplayName("Id Hayvan Ref Padok No"), Expression("jIdHayvanRef.[PadokNo]")]
        public Int32? IdHayvanRefPadokNo
        {
            get { return Fields.IdHayvanRefPadokNo[this]; }
            set { Fields.IdHayvanRefPadokNo[this] = value; }
        }

        [DisplayName("Id Gorevli Ref Yetki"), Expression("jIdGorevliRef.[Yetki]")]
        public Int32? IdGorevliRefYetki
        {
            get { return Fields.IdGorevliRefYetki[this]; }
            set { Fields.IdGorevliRefYetki[this] = value; }
        }

        [DisplayName("Id Gorevli Ref Tc"), Expression("jIdGorevliRef.[TC]")]
        public Int32? IdGorevliRefTc
        {
            get { return Fields.IdGorevliRefTc[this]; }
            set { Fields.IdGorevliRefTc[this] = value; }
        }

        [DisplayName("Id Gorevli Ref Ad"), Expression("jIdGorevliRef.[Ad]")]
        public String IdGorevliRefAd
        {
            get { return Fields.IdGorevliRefAd[this]; }
            set { Fields.IdGorevliRefAd[this] = value; }
        }

        [DisplayName("Id Gorevli Ref Soyad"), Expression("jIdGorevliRef.[Soyad]")]
        public String IdGorevliRefSoyad
        {
            get { return Fields.IdGorevliRefSoyad[this]; }
            set { Fields.IdGorevliRefSoyad[this] = value; }
        }

        [DisplayName("Id Gorevli Ref Adres"), Expression("jIdGorevliRef.[Adres]")]
        public String IdGorevliRefAdres
        {
            get { return Fields.IdGorevliRefAdres[this]; }
            set { Fields.IdGorevliRefAdres[this] = value; }
        }

        [DisplayName("Id Gorevli Ref Telefon"), Expression("jIdGorevliRef.[Telefon]")]
        public Int32? IdGorevliRefTelefon
        {
            get { return Fields.IdGorevliRefTelefon[this]; }
            set { Fields.IdGorevliRefTelefon[this] = value; }
        }

        [DisplayName("Id Gorevli Ref Mail"), Expression("jIdGorevliRef.[Mail]")]
        public String IdGorevliRefMail
        {
            get { return Fields.IdGorevliRefMail[this]; }
            set { Fields.IdGorevliRefMail[this] = value; }
        }

        [DisplayName("Id Gorevli Ref Isten Cikti Mi"), Expression("jIdGorevliRef.[IstenCiktiMi]")]
        public Boolean? IdGorevliRefIstenCiktiMi
        {
            get { return Fields.IdGorevliRefIstenCiktiMi[this]; }
            set { Fields.IdGorevliRefIstenCiktiMi[this] = value; }
        }

        [DisplayName("Id Asi Ref Asi Adi"), Expression("jIdAsiRef.[AsiAdi]")]
        public String IdAsiRefAsiAdi
        {
            get { return Fields.IdAsiRefAsiAdi[this]; }
            set { Fields.IdAsiRefAsiAdi[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.IdAsilama; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public AsilamaRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field IdAsilama;
            public Int32Field IdHayvanRef;
            public DateTimeField AsilamaTarihi;
            public Int32Field IdGorevliRef;
            public BooleanField AsilamaYapildimi;
            public Int32Field IdAsiRef;

            public StringField IdHayvanRefKupeNo;
            public Int32Field IdHayvanRefCinsiyet;
            public Int32Field IdHayvanRefIdHayvanTuruRef;
            public Int32Field IdHayvanRefIdHayvanCinsiRef;
            public BooleanField IdHayvanRefSatildiDurumu;
            public BooleanField IdHayvanRefKesildiDurumu;
            public BooleanField IdHayvanRefTelefDurumu;
            public StringField IdHayvanRefAciklama;
            public DateTimeField IdHayvanRefHayvanDogumTarihi;
            public Int32Field IdHayvanRefPadokNo;

            public Int32Field IdGorevliRefYetki;
            public Int32Field IdGorevliRefTc;
            public StringField IdGorevliRefAd;
            public StringField IdGorevliRefSoyad;
            public StringField IdGorevliRefAdres;
            public Int32Field IdGorevliRefTelefon;
            public StringField IdGorevliRefMail;
            public BooleanField IdGorevliRefIstenCiktiMi;

            public StringField IdAsiRefAsiAdi;

            public RowFields()
                : base("[dbo].[Asilama]")
            {
                LocalTextPrefix = "Menuler.Asilama";
            }
        }
    }
}