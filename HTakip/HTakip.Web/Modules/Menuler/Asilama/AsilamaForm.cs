﻿
namespace HTakip.Menuler.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Menuler.Asilama")]
    [BasedOnRow(typeof(Entities.AsilamaRow))]
    public class AsilamaForm
    {
        public Int32 IdHayvanRef { get; set; }
        public DateTime AsilamaTarihi { get; set; }
        public Int32 IdGorevliRef { get; set; }
        public Boolean AsilamaYapildimi { get; set; }
        public Int32 IdAsiRef { get; set; }
    }
}