﻿
namespace HTakip.Menuler.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Menuler.Asilama")]
    [BasedOnRow(typeof(Entities.AsilamaRow))]
    public class AsilamaColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 IdAsilama { get; set; }
        public Int32 IdHayvanRef { get; set; }
        public DateTime AsilamaTarihi { get; set; }
        public Int32 IdGorevliRef { get; set; }
        public Boolean AsilamaYapildimi { get; set; }
        public Int32 IdAsiRef { get; set; }
    }
}