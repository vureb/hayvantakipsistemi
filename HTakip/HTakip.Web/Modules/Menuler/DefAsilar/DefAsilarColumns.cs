﻿
namespace HTakip.Menuler.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Menuler.DefAsilar")]
    [BasedOnRow(typeof(Entities.DefAsilarRow))]
    public class DefAsilarColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 IdAsi { get; set; }
        [EditLink]
        public String AsiAdi { get; set; }
    }
}