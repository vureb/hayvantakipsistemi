﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Menuler/DefAsilar", typeof(HTakip.Menuler.Pages.DefAsilarController))]

namespace HTakip.Menuler.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Menuler/DefAsilar"), Route("{action=index}")]
    public class DefAsilarController : Controller
    {
        [PageAuthorize("Administration")]
        public ActionResult Index()
        {
            return View("~/Modules/Menuler/DefAsilar/DefAsilarIndex.cshtml");
        }
    }
}