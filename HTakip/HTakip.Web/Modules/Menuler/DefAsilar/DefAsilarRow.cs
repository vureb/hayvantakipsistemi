﻿
namespace HTakip.Menuler.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Northwind"), DisplayName("Def_Asilar"), InstanceName("Def_Asilar"), TwoLevelCached]
    [ReadPermission("Administration")]
    [ModifyPermission("Administration")]
    public sealed class DefAsilarRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id Asi"), PrimaryKey]
        public Int32? IdAsi
        {
            get { return Fields.IdAsi[this]; }
            set { Fields.IdAsi[this] = value; }
        }

        [DisplayName("Asi Adi"), Size(50), QuickSearch]
        public String AsiAdi
        {
            get { return Fields.AsiAdi[this]; }
            set { Fields.AsiAdi[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.IdAsi; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.AsiAdi; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public DefAsilarRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field IdAsi;
            public StringField AsiAdi;

            public RowFields()
                : base("[dbo].[Def_Asilar]")
            {
                LocalTextPrefix = "Menuler.DefAsilar";
            }
        }
    }
}