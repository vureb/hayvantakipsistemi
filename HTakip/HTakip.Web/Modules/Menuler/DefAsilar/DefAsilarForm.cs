﻿
namespace HTakip.Menuler.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Menuler.DefAsilar")]
    [BasedOnRow(typeof(Entities.DefAsilarRow))]
    public class DefAsilarForm
    {
        public String AsiAdi { get; set; }
    }
}