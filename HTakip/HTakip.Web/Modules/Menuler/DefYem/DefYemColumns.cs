﻿
namespace HTakip.Menuler.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Menuler.DefYem")]
    [BasedOnRow(typeof(Entities.DefYemRow))]
    public class DefYemColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 IdYem { get; set; }
        [EditLink]
        public String YemAdi { get; set; }
    }
}