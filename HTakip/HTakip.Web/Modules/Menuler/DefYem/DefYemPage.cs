﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Menuler/DefYem", typeof(HTakip.Menuler.Pages.DefYemController))]

namespace HTakip.Menuler.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Menuler/DefYem"), Route("{action=index}")]
    public class DefYemController : Controller
    {
        [PageAuthorize("Administration")]
        public ActionResult Index()
        {
            return View("~/Modules/Menuler/DefYem/DefYemIndex.cshtml");
        }
    }
}