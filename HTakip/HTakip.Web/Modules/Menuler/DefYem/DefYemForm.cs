﻿
namespace HTakip.Menuler.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Menuler.DefYem")]
    [BasedOnRow(typeof(Entities.DefYemRow))]
    public class DefYemForm
    {
        public String YemAdi { get; set; }
    }
}