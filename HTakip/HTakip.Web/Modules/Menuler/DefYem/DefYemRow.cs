﻿
namespace HTakip.Menuler.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Northwind"), DisplayName("Def_Yem"), InstanceName("Def_Yem"), TwoLevelCached]
    [ReadPermission("Administration")]
    [ModifyPermission("Administration")]
    public sealed class DefYemRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id Yem"), PrimaryKey]
        public Int32? IdYem
        {
            get { return Fields.IdYem[this]; }
            set { Fields.IdYem[this] = value; }
        }

        [DisplayName("Yem Adi"), Size(50), QuickSearch]
        public String YemAdi
        {
            get { return Fields.YemAdi[this]; }
            set { Fields.YemAdi[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.IdYem; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.YemAdi; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public DefYemRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field IdYem;
            public StringField YemAdi;

            public RowFields()
                : base("[dbo].[Def_Yem]")
            {
                LocalTextPrefix = "Menuler.DefYem";
            }
        }
    }
}