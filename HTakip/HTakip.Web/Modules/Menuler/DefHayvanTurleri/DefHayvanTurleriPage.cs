﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Menuler/DefHayvanTurleri", typeof(HTakip.Menuler.Pages.DefHayvanTurleriController))]

namespace HTakip.Menuler.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Menuler/DefHayvanTurleri"), Route("{action=index}")]
    public class DefHayvanTurleriController : Controller
    {
        [PageAuthorize("Administration")]
        public ActionResult Index()
        {
            return View("~/Modules/Menuler/DefHayvanTurleri/DefHayvanTurleriIndex.cshtml");
        }
    }
}