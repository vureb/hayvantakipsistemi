﻿
namespace HTakip.Menuler.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Menuler.DefHayvanTurleri")]
    [BasedOnRow(typeof(Entities.DefHayvanTurleriRow))]
    public class DefHayvanTurleriColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 IdHayvanTuru { get; set; }
        [EditLink]
        public String HayvanTuruAdi { get; set; }
    }
}