﻿
namespace HTakip.Menuler.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Northwind"), DisplayName("Def_HayvanTurleri"), InstanceName("Def_HayvanTurleri"), TwoLevelCached]
    [ReadPermission("Administration")]
    [ModifyPermission("Administration")]
    public sealed class DefHayvanTurleriRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id Hayvan Turu"), PrimaryKey]
        public Int32? IdHayvanTuru
        {
            get { return Fields.IdHayvanTuru[this]; }
            set { Fields.IdHayvanTuru[this] = value; }
        }

        [DisplayName("Hayvan Turu Adi"), Size(50), QuickSearch]
        public String HayvanTuruAdi
        {
            get { return Fields.HayvanTuruAdi[this]; }
            set { Fields.HayvanTuruAdi[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.IdHayvanTuru; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.HayvanTuruAdi; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public DefHayvanTurleriRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field IdHayvanTuru;
            public StringField HayvanTuruAdi;

            public RowFields()
                : base("[dbo].[Def_HayvanTurleri]")
            {
                LocalTextPrefix = "Menuler.DefHayvanTurleri";
            }
        }
    }
}