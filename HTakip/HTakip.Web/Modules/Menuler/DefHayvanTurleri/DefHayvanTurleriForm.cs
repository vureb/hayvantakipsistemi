﻿
namespace HTakip.Menuler.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Menuler.DefHayvanTurleri")]
    [BasedOnRow(typeof(Entities.DefHayvanTurleriRow))]
    public class DefHayvanTurleriForm
    {
        public String HayvanTuruAdi { get; set; }
    }
}