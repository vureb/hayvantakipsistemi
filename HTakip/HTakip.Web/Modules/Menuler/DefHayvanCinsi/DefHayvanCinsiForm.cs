﻿
namespace HTakip.Menuler.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Menuler.DefHayvanCinsi")]
    [BasedOnRow(typeof(Entities.DefHayvanCinsiRow))]
    public class DefHayvanCinsiForm
    {
        public Int32 IdHayvanTuruRef { get; set; }
        public String HayvanCinsiAdi { get; set; }
    }
}