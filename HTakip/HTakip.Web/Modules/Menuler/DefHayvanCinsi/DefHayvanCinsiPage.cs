﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Menuler/DefHayvanCinsi", typeof(HTakip.Menuler.Pages.DefHayvanCinsiController))]

namespace HTakip.Menuler.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Menuler/DefHayvanCinsi"), Route("{action=index}")]
    public class DefHayvanCinsiController : Controller
    {
        [PageAuthorize("Administration")]
        public ActionResult Index()
        {
            return View("~/Modules/Menuler/DefHayvanCinsi/DefHayvanCinsiIndex.cshtml");
        }
    }
}