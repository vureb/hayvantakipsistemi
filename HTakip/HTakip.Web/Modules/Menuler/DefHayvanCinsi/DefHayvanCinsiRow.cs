﻿
namespace HTakip.Menuler.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Northwind"), DisplayName("Def_HayvanCinsi"), InstanceName("Def_HayvanCinsi"), TwoLevelCached]
    [ReadPermission("Administration")]
    [ModifyPermission("Administration")]
    public sealed class DefHayvanCinsiRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id Hayvan Cinsi"), PrimaryKey]
        public Int32? IdHayvanCinsi
        {
            get { return Fields.IdHayvanCinsi[this]; }
            set { Fields.IdHayvanCinsi[this] = value; }
        }

        [DisplayName("Id Hayvan Turu Ref"), ForeignKey("[dbo].[Def_HayvanTurleri]", "IdHayvanTuru"), LeftJoin("jIdHayvanTuruRef"), TextualField("IdHayvanTuruRefHayvanTuruAdi")]
        public Int32? IdHayvanTuruRef
        {
            get { return Fields.IdHayvanTuruRef[this]; }
            set { Fields.IdHayvanTuruRef[this] = value; }
        }

        [DisplayName("Hayvan Cinsi Adi"), Size(50), QuickSearch]
        public String HayvanCinsiAdi
        {
            get { return Fields.HayvanCinsiAdi[this]; }
            set { Fields.HayvanCinsiAdi[this] = value; }
        }

        [DisplayName("Id Hayvan Turu Ref Hayvan Turu Adi"), Expression("jIdHayvanTuruRef.[HayvanTuruAdi]")]
        public String IdHayvanTuruRefHayvanTuruAdi
        {
            get { return Fields.IdHayvanTuruRefHayvanTuruAdi[this]; }
            set { Fields.IdHayvanTuruRefHayvanTuruAdi[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.IdHayvanCinsi; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.HayvanCinsiAdi; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public DefHayvanCinsiRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field IdHayvanCinsi;
            public Int32Field IdHayvanTuruRef;
            public StringField HayvanCinsiAdi;

            public StringField IdHayvanTuruRefHayvanTuruAdi;

            public RowFields()
                : base("[dbo].[Def_HayvanCinsi]")
            {
                LocalTextPrefix = "Menuler.DefHayvanCinsi";
            }
        }
    }
}