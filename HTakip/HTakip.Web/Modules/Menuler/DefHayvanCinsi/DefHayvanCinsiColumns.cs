﻿
namespace HTakip.Menuler.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Menuler.DefHayvanCinsi")]
    [BasedOnRow(typeof(Entities.DefHayvanCinsiRow))]
    public class DefHayvanCinsiColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 IdHayvanCinsi { get; set; }
        public Int32 IdHayvanTuruRef { get; set; }
        [EditLink]
        public String HayvanCinsiAdi { get; set; }
    }
}