﻿
namespace HTakip.Menuler.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Northwind"), DisplayName("Def_Hastalik"), InstanceName("Def_Hastalik"), TwoLevelCached]
    [ReadPermission("Administration")]
    [ModifyPermission("Administration")]
    public sealed class DefHastalikRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id Hastalik"), PrimaryKey]
        public Int32? IdHastalik
        {
            get { return Fields.IdHastalik[this]; }
            set { Fields.IdHastalik[this] = value; }
        }

        [DisplayName("Hastalik Adi"), Size(50), QuickSearch]
        public String HastalikAdi
        {
            get { return Fields.HastalikAdi[this]; }
            set { Fields.HastalikAdi[this] = value; }
        }

        [DisplayName("Aciklama")]
        public String Aciklama
        {
            get { return Fields.Aciklama[this]; }
            set { Fields.Aciklama[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.IdHastalik; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.HastalikAdi; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public DefHastalikRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field IdHastalik;
            public StringField HastalikAdi;
            public StringField Aciklama;

            public RowFields()
                : base("[dbo].[Def_Hastalik]")
            {
                LocalTextPrefix = "Menuler.DefHastalik";
            }
        }
    }
}