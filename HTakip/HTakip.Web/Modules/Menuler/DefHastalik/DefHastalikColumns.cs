﻿
namespace HTakip.Menuler.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Menuler.DefHastalik")]
    [BasedOnRow(typeof(Entities.DefHastalikRow))]
    public class DefHastalikColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 IdHastalik { get; set; }
        [EditLink]
        public String HastalikAdi { get; set; }
        public String Aciklama { get; set; }
    }
}