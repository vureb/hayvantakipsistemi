﻿
namespace HTakip.Menuler.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Menuler.DefHastalik")]
    [BasedOnRow(typeof(Entities.DefHastalikRow))]
    public class DefHastalikForm
    {
        public String HastalikAdi { get; set; }
        public String Aciklama { get; set; }
    }
}