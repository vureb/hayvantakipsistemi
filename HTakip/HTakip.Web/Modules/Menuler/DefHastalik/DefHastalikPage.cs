﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Menuler/DefHastalik", typeof(HTakip.Menuler.Pages.DefHastalikController))]

namespace HTakip.Menuler.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Menuler/DefHastalik"), Route("{action=index}")]
    public class DefHastalikController : Controller
    {
        [PageAuthorize("Administration")]
        public ActionResult Index()
        {
            return View("~/Modules/Menuler/DefHastalik/DefHastalikIndex.cshtml");
        }
    }
}