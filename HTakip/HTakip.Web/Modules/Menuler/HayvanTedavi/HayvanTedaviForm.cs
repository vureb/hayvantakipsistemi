﻿
namespace HTakip.Menuler.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Menuler.HayvanTedavi")]
    [BasedOnRow(typeof(Entities.HayvanTedaviRow))]
    public class HayvanTedaviForm
    {
        public Int32 IdHayvanRef { get; set; }
        public DateTime TedaviTarihi { get; set; }
        public Int32 IdHastalikRef { get; set; }
        public String Aciklama { get; set; }
    }
}