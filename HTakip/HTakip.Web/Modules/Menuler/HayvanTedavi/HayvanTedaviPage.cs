﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Menuler/HayvanTedavi", typeof(HTakip.Menuler.Pages.HayvanTedaviController))]

namespace HTakip.Menuler.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Menuler/HayvanTedavi"), Route("{action=index}")]
    public class HayvanTedaviController : Controller
    {
        [PageAuthorize("Administration")]
        public ActionResult Index()
        {
            return View("~/Modules/Menuler/HayvanTedavi/HayvanTedaviIndex.cshtml");
        }
    }
}