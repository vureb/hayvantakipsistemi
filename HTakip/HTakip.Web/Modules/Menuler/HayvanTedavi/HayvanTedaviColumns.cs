﻿
namespace HTakip.Menuler.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Menuler.HayvanTedavi")]
    [BasedOnRow(typeof(Entities.HayvanTedaviRow))]
    public class HayvanTedaviColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 IdTedavi { get; set; }
        public Int32 IdHayvanRef { get; set; }
        public DateTime TedaviTarihi { get; set; }
        public Int32 IdHastalikRef { get; set; }
        [EditLink]
        public String Aciklama { get; set; }
    }
}